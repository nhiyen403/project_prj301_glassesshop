<%-- 
    Document   : DisplayProduct
    Created on : Oct 25, 2023, 4:47:20 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Product, java.util.Vector"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String[] product = vecPro.get(0);
        %>
        <div class="container">
            <h3 class="pb-3 mb-4 font-italic border-bottom">
                Product ID : <%=product[0]%>
            </h3>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="thumbnail">
                    <img src="<%=product[7]%>" alt="Thumbnail" style="width: 350px; height: 300px;">
                    <div class="caption">
                        <h3><%=product[1]%></h3>
                        <h4>Price: <%=product[2]%>đ</h4>
                        <p><%=product[6]%></p>
                        <p><a class="btn btn-success" href="orderDetailURL?service=addToCart&ProductID=<%=product[0]%>" role="button">Add to cart</a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
