
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="jakarta.servlet.http.HttpSession" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Shop Homepage</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <%
            String username = (String)session.getAttribute("username");
            if(username == null){
                username ="";
            }
        %>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <div class="navbar-header">
                    <p class="navbar-text navbar-right" style="font-size: 20px">HE172239 Vương Thị Yến Nhi</p>

                </div>
                <%
                    if(!username.equals("")){
                %>            
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <p class="navbar-text navbar-right" style="font-size: 20px; color:green">Welcome: <%=username%> </p>
                    </ul>                       
                </div>
                <%}%> 
   
            </div>
        </nav>
        <!---->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="homeURL">Home</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                    </ul>
                    <form class="navbar-form navbar-left" action="productURL" method="post">
                        <input type="hidden" name ="service" value="searchProduct">
                        <div class="form-group">
                            <input type="text" name="ProductName" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="orderDetailURL?service=showCart"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Show Cart</a></li>
                        <li><a href="customerURL?service=login">Login </a></li>
                        <%
                            if(!username.equals("")){
                        %>
                        <li><a href="customerURL?service=logout">Logout</a></li>
                        <%}%>
                        <%
                            if(!username.equals("")){
                        %>
                        <li><a href="adminURL?service=logout">Logout</a></li>
                        <%}%>
                        
                        <li><a href="register.jsp">Register </a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Glasses Shop</h1>
                    <p class="lead fw-normal text-white-50 mb-0">Thương hiệu kính mắt của sự tử tế</p>
                </div>
            </div>
        </header>
    </body>
</html>
