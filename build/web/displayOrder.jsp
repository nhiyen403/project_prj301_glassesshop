<%-- 
    Document   : displayOrder
    Created on : Oct 31, 2023, 5:35:27 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.util.Vector"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Vector<String[]> vecOr = (Vector<String[]>)request.getAttribute("dataOr");
            String message =(String)request.getAttribute("message");
        %>
        <p style="color:red"><%=message%></p>
        <table class="table table-striped">
            <tr style="background-color: wheat;">
                <th>OrderID</th>
                <th>Customer Name</th>
                <th>Order Date</th>
                <th>Status</th>
                <th>Total</th>
                <th>View</th>
            </tr>
            <% 
                for (String[] order : vecOr) {
                    double total = Double.parseDouble(order[4]);
            %>    
            <tr>
                <td><%= order[0] %></td>
                <td><%= order[1] %></td>
                <td><%= order[2] %></td>
                <td><%= order[3] %></td>
                <td><%= total%>đ</td>
                <td><a href="orderDetailURL?service=displayOrderDetail&orderID=<%=order[0]%>">view</a></td>
            </tr>
            <%}%>  
        </table>
    </body>
</html>
