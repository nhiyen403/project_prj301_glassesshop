<%-- 
    Document   : createBill
    Created on : Oct 29, 2023, 8:59:07 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet,java.util.Vector,entity.Customer" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Vector<Customer> vecCus = (Vector)request.getAttribute("vecCus");
            Customer customer = vecCus.get(0);
            String message =(String)request.getAttribute("message");
        %>
        <p style="color:red"><%=message%></p>
        <form action="orderURL" method="post">
            <table cellpadding="5" class="table">
                <input type="hidden" name ="service" value="createBill">
                <input type="hidden" name ="CustomerID" value="<%=customer.getCustomerID()%>">
                <tr>
                    <td>Customer:</td>
                    <td><input type="text" name="cusName" value="<%=customer.getCustomerName()%>" readonly=""></td>
                </tr>
                <tr>
                    <td>OrderDate:</td>
                    <td><input type="date" name="OrderDate" value=""></td>
                </tr>
            </table>
            <p> <input type="submit" name="submit" value="Continue" />
                <input type="reset" value="Clear" />  
        </form>
    </body>
</html>
