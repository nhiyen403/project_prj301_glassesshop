<%-- 
    Document   : content
    Created on : Oct 6, 2023, 11:55:12 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Product, java.util.Vector"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/styleP1.css">
        <link rel="stylesheet" href="css/styleProduct.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <%
            Vector<String[]> vecPro = (Vector)request.getAttribute("dataPro");
        %>
        <div class="content">
            <%
            if(check.equals("0")){
            %>
            <table class="table table-striped">
                <tr style="background-color: wheat;">
                    <th>ProductID</th>
                    <th>ProductName</th>
                    <th>Category</th>
                    <th>Supplier</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Picture</th>
                    <th>add to cart</th>
                </tr>
                <% 
                    for (String[] product : vecPro) {
                %>    
                <tr>
                    <td><%= product[0] %></td>
                    <td><a href="productURL?service=showProductDetail&productID=<%=product[0]%>"><%= product[1] %></td>
                    <td><%= product[4] %></td>
                    <td><%= product[5] %></td>
                    <td><%= product[2] %>đ</td>
                    <td><%= product[3] %></td>
                    <td><img src="<%=product[7]%>" alt="" style="width: 60px; height: 50px;"></td>
                    <td><a href="orderDetailURL?service=addToCart&ProductID=<%=product[0]%>">add to cart</a></td>
                </tr>
                <%}%>  
            </table>
            <%} else if(check.equals("1")){//display product detail%>
            <%@ include file="displayProductDetail.jsp"%>
            <!--admin-->
            <%}else if(check.equals("3")){%>
            <%@ include file="displayCustomer.jsp"%>
            <%}else if(check.equals("4")){%>
            <%@ include file="displayProduct.jsp"%>
            <%}else if(check.equals("5")){%>
            <%@ include file="displayOrder.jsp"%>
            <%}else if(check.equals("6")){%>
            <%@ include file="displayOrderDetail.jsp"%>
            <%}%>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    </body>
</html>
