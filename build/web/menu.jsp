<%-- 
    Document   : menu
    Created on : Oct 6, 2023, 11:45:09 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Supplier, java.util.Vector"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/styleP1.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <%
            Vector<Supplier> vecSup = (Vector)request.getAttribute("dataSup");
            int tag = Integer.parseInt((String)request.getAttribute("tag"));
        %>
        <div class="sidebar">
            <div class="list-group">
                <div class="list-group-item active">
                    Category
                </div>
                <%if(check.equals("0") || check.equals("1")){%>
                    <%for (Supplier supplier : vecSup) {%>
                    <a href="productURL?service=viewProductBySup&SupplierID=<%=supplier.getSupplierID()%>" class="list-group-item <%= tag==supplier.getSupplierID()?"list-group-item-info":""%>" ><%=supplier.getCompanyName()%></a>
                    <%}%>
                <%} else {%>
                    <a href="productURL?service=viewAllProduct" class="list-group-item <%= tag==1?"list-group-item-info":""%>" >Product Manager</a>
                    <a href="customerURL?service=viewAllCustomer" class="list-group-item <%= tag==2?"list-group-item-info":""%>" >Customer Manager</a>
                    <a href="orderURL?service=viewAllOrder" class="list-group-item <%= tag==3?"list-group-item-info":""%>" >Bill Manager</a>
                <%}%>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    </body>
</html>
