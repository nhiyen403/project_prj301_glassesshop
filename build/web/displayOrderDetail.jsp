<%-- 
    Document   : displayOrderDetail
    Created on : Nov 1, 2023, 1:29:34 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Vector<String[]> vecOrDe = (Vector<String[]>)request.getAttribute("dataOrDe");
            String message = (String)request.getAttribute("message");
            String[] od = vecOrDe.get(0);
            String orID = od[0];          
            String orderDate = od[1];
            String CusName = od[2];
            String status = od[7];
            double GrandTotal = 0;
            int no = 1;
        %>
        <dl class="dl-horizontal">
            <dt>OrderID:</dt><dd><%=od[0]%></dd>
            <dt>Order Date:</dt><dd><%=od[1]%></dd>
            <dt>Customer Name:</dt><dd><%=od[2]%></dd>
        </dl>
        <table class="table table-striped">
            <tr style="background-color: wheat;">
                <th>No</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>SubTotal</th>
            </tr>
            <% 
                for (String[] orderDetail : vecOrDe) {
                    double subtotal = Double.parseDouble(orderDetail[6]);
                    double price = Double.parseDouble(orderDetail[5]);
            %>    
            <tr>
                <td><%= no%></td>
                <td><%= orderDetail[3] %></td>
                <td><%= orderDetail[4] %></td>
                <td><%= price %>đ
                <td><%= subtotal%>đ</td>
            </tr>
            <%
                no +=1;
                GrandTotal += subtotal;
                }
            %> 
            <tr>
                <td colspan="5" align="right"> Grand Total: <%=GrandTotal%>đ</td>
            </tr>
        </table>
        <form action="orderURL">
            <input type="hidden" name="OrderID" value="<%=orID%>">
            <input type="hidden" name="service" value="updateOrder">
            <input type="hidden" name="checkDone" value="<%=status%>">
            <div>Status:
                <input type="radio" name="status" value="Wait" <%=status.equals("Wait")?"checked":""%> /> Wait
                <input type="radio" name="status" value="Process" <%=status.equals("Process")?"checked":""%>/> Process
                <input type="radio" name="status" value="Done" <%=status.equals("Done")?"checked":""%>/> Done
            </div>
            <button type="sunmit">Update</button>
        </form>
        <div><%=message%></div>
    </body>
</html>
