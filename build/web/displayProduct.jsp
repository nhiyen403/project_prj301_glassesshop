<%-- 
    Document   : displayProduct
    Created on : Oct 31, 2023, 5:34:46 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Vector<String[]> vec = (Vector<String[]>)request.getAttribute("dataPro");
            String message =(String)request.getAttribute("message");
            if (message == null) {
                message = "";
            }
        %>
        <p style="color:red"><%=message%></p>
        <a href="productURL?service=insertProduct">Insert product</a><br>
        <form action="productURL" method="post">
            <table cellpadding="5" class="table">
                <input type="hidden" name ="service" value="searchByPrice">
                <tr>
                    <td>Min Price: </td>
                    <td><input type="text" name="MinPrice" placeholder="Enter Min Price"></td>
                </tr>
                <tr>
                    <td>Max Price: </td>
                    <td><input type="text" name="MaxPrice" placeholder="Enter Max Price"></td>
                </tr>
            </table>
            <p> <input type="submit" name="submit" value="Search" />
                <input type="reset" value="Clear" />  
        </form>
        <table class="table table-striped">
            <tr style="background-color: wheat;">
                <th>ProductID</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Status</th>
                <th>Category</th>
                <th>Supplier</th>
                <th>Description</th>
                <th>Picture</th>
                <th>update</th>
                <th>delete</th>
            </tr>
            <% 
                for (String[] product : vec) {
            %>    
            <tr>
                <td><%= product[0] %></td>
                <td><%= product[1] %></td>
                <td><%= product[2] %></td>
                <td><%= product[3] %></td>
                <td><%= product[4] %></td>
                <td><%= product[5] %></td>
                <td><%= product[6] %></td>
                <td><img src="<%=product[7]%>" alt="" style="width: 50px; height: 60px;"></td>
                <td><a href="productURL?service=updateProduct&ProductID=<%=product[0]%>">update</a></td>
                <td><a href="productURL?service=deleteProduct&ProductID=<%=product[0]%>">delete</a></td>
            </tr>
            <%}%>  
        </table>
    </body>
</html>
