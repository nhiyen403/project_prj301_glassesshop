<%-- 
    Document   : index
    Created on : Oct 6, 2023, 11:15:38 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/styleP1.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <%String check = (String)request.getAttribute("check");%>
        <!--header-->
       <div style="background-image: url('https://cosp.com.vn/uploaded/thu/kinh-mat/linh-anh/thiet-ke-shop-thoi-trang-kinh-mat-linh-anh-5.jpg'); background-size: cover; background-color: rgba(0, 0, 0, 0.5);
  padding: 100px;font-size: 2rem;">
            <%@ include file="panner.jsp" %>
        </div>
        <!--main-->
        <div class="main">
            <!--menu-->
            <%@ include file="menu.jsp" %>
            <!--content-->
            <%@ include file="content.jsp" %>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    </body>
</html>
