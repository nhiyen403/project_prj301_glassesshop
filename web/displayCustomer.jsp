<%-- 
    Document   : displayCustomer
    Created on : Oct 31, 2023, 5:35:07 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.util.Vector"%>
<%@page import= "entity.Customer"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Vector<Customer> vec = (Vector)request.getAttribute("dataCus");
            String message =(String)request.getAttribute("message");
        %>
        <p style="color:red"><%=message%></p>
        <a href="customerURL?service=insertCustomer">Insert customer</a><br>
        <table class="table table-striped">
            <tr style="background-color: wheat;">
                <th>CustomerID</th>
                <th>Customer Name</th>
                <th>Username</th>
                <th>Password</th>
                <th>Address</th>
                <th>Phone</th>
            </tr>
            <%for (Customer customer : vec) {%>
            <tr>
                <td><%=customer.getCustomerID()%></td>
                <td><%=customer.getCustomerName()%></td>
                <td><%=customer.getUsername()%></td>
                <td><%=customer.getPassword()%></td>
                <td><%=customer.getAddress()%></td>
                <td><%=customer.getPhone()%></td>
            </tr>
            <%}%>
        </table>
    </body>
</html>
