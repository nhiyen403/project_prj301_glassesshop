<%-- 
    Document   : login.jsp
    Created on : Oct 7, 2023, 12:14:56 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                background: url('your-background-image.jpg') no-repeat center center fixed;
                background-size: cover;
            }
            .login-form {
                width: 400px;
                margin: 50px auto;
                font-size: 15px;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 48px;
                border-radius: 2px;
            }
            .btn {
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                display: block;
                margin: 0 auto;
            }
        </style>
    </head>
    <body>
        <div class="login-form">
            <form action ="adminURL" method ="post">
                <input type="hidden" name ="service" value="login">
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" name="username" value="" class="form-control" placeholder="User Name">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value="" class="form-control" placeholder="Password">
                </div>
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Login</button>
                <div class="clearfix">
                    <a href="#" class="float-right">Forgot Password?</a>
                    <p class="text-left"><a href="homeURL">Home</a></p>
                </div> 
            </form>
            <%
                String message = (String)request.getAttribute("message");
                if(message == null){
                    message = "";
                }
            %>
            <p><%=message%></p>
        </div>
    </body>
</html>
