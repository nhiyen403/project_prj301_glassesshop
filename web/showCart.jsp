
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Vector"%>
<%@page import="entity.OrderDetail"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Cart</title>
        <link rel="stylesheet" href="css/styleP1.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <style>
            body{
                margin-top:20px;
                background:#eee;
            }
            .ui-w-40 {
                width: 40px !important;
                height: auto;
            }

            .card{
                box-shadow: 0 1px 15px 1px rgba(52,40,104,.08);
            }

            .ui-product-color {
                display: inline-block;
                overflow: hidden;
                margin: .144em;
                width: .875rem;
                height: .875rem;
                border-radius: 10rem;
                -webkit-box-shadow: 0 0 0 1px rgba(0,0,0,0.15) inset;
                box-shadow: 0 0 0 1px rgba(0,0,0,0.15) inset;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <form action="orderDetailURL">
            <input type="hidden" name="service" value="updateCart">
            <div class="container px-3 my-5 clearfix">
                <!-- Shopping cart table -->
                <div class="card">
                    <div class="card-header">
                        <h2>Shopping Cart</h2>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered m-0">
                                <thead>
                                    <tr>
                                        <!-- Set columns width -->
                                        <th class="text-left py-3 px-4" style="width: 200px;">ID</th>
                                        <th class="text-left py-3 px-4" style="width: 250px;">Product Name</th>
                                        <th class="text-center py-3 px-4" style="width: 100px;">Quantity</th>
                                        <th class="text-right py-3 px-4" style="width: 200px;">Price</th>
                                        <th class="text-right py-3 px-4" style="width: 200px;">Total Price</th>
<!--                                        <th class="text-center align-middle py-3 px-0" style="width: 100px;">Remove<a href="#" class="shop-tooltip float-none text-light" title="" data-original-title="Clear cart"><i class="ino ion-md-trash"></i></a></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        java.util.Enumeration em = session.getAttributeNames();
                                        double grandTotal = 0;
                                    //  getkeys()
                                        while(em.hasMoreElements()){
                                            String id= em.nextElement().toString(); //get key
                                            if(!id.equals("username") && !id.equals("password")){
                                                String[] odBill=(String[])session.getAttribute(id);
                                                String productName = odBill[1];
                                                int quantity = Integer.parseInt(odBill[2]);
                                                double price = Double.parseDouble(odBill[3]);
                                    %>
                                    <tr>
                                        <td><%= id%></td>
                                        <td><%= productName%></td>
                                        <td><input type="number" name="<%=id%>" value="<%=quantity%>"></td>
                                        <td><%= price%>đ</td>
                                        <td><%= quantity * price %>đ</td>
<!--                                        <td><a href="orderDetailURL?service=deleteBill&id=<%=id%>" class="btn-danger">Remove</a></td>-->
                                    </tr>
                                    <%
                                            grandTotal += quantity * price;
                                            }
                                        }
                                    %>
                                    <tr>
                                        <td><button type="submit" name="submit" class=" btn-success">Update</button></td>
                                        <td colspan="4" align="right"> Grand Total: <%=grandTotal%>đ</td>
<!--                                        <td><a href="orderDetailURL?service=removeAll" class=" btn-danger">Remove All</td>-->
                                    </tr>
                            </table>
                            </form>
                            <% String message =(String)request.getAttribute("message"); 
                                if(message == null){
                                    message="";
                                }
                            %>
                            <p style="color:red"><%=message%></p>
                            <button class="btn btn-lg btn-default md-btn-flat mt-2 mr-3"><a href="orderDetailURL?service=checkOut">Check-out</button>
                            <button class="btn btn-lg btn-primary mt-2"><a href="homeURL" style="color:white">Home Page</button>
                        </div>
                    </div>
                </div>
            </div>                     
    </body>
</html>
