<%-- 
    Document   : insertProduct
    Created on : Nov 8, 2023, 2:26:34 PM
    Author     : vuong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert Product</title>
    </head>
    <body>
        <%
            ResultSet rsCate = (ResultSet)request.getAttribute("rsCate");
            ResultSet rsSup = (ResultSet)request.getAttribute("rsSup");
        %>
        <form action="productURL" method="post">
            <table cellpadding="5">
                <input type="hidden" name ="service" value="insertProduct">
                <tr>
                    <td>Product ID:</td>
                    <td><input type="text" name="ProductID" value=""></td>
                </tr>
                <tr>
                    <td>Product Name:</td>
                    <td><input type="text" name="ProductName" value=""></td>
                </tr>
                <tr>
                    <td>Price:</td>
                    <td><input type="text" name="Price" value=""></td>
                </tr><!-- comment -->
                <tr>
                    <td>Status:</td>
                    <td><input type="text" name="Status" value=""></td>
                </tr>
                
                <tr>
                    <td>
                        <label for="CategoryID">Category: </label>
                    </td>
                    <td>
                        <select name="CategoryID" id="CategoryID">
                            <%while (rsCate.next()){%>
                            <option value="<%=rsCate.getInt(1)%>"><%=rsCate.getString(2)%></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="SupplierID">Supplier: </label>
                    </td>
                    <td>
                        <select name="SupplierID" id="SupplierID">
                            <%while(rsSup.next()){%>
                            <option value="<%=rsSup.getInt(1)%>"><%=rsSup.getString(2)%></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
            </table>
            <p> <input type="submit" name="submit" value="insert"/>
                <input type="reset" value="Clear"/>  
        </form>
    </body>
</html>
