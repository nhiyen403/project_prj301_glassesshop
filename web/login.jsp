
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Login Form</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            .background-image {
                margin: 0;
                padding: 0;
                background: url('https://htmediagroup.vn/wp-content/uploads/2021/07/Kinh-mat-2.jpg') no-repeat center center fixed;
                background-size: cover;
            }

            .login-form {
                width: 450px;
                margin: 180px auto;
                font-size: 18px;
                background: rgba(255, 255, 255, 0.8); /* Màu nền của biểu mẫu đăng nhập */
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {
                font-size: 15px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="background-image">
        <div class="login-form">
            <form action="customerURL" method="post">
                <h2 class="text-center">Log in</h2> 
                <input type="hidden" name ="service" value="login">
                <div class="form-group">
                    <label>UserName</label>
                    <input type="text" class="form-control" name="username" placeholder="Username" required="required">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block">Log in</button>
                </div>
                <p class="text-center"><a href="customerURL?service=addCustomer">Create an Account</a></p>
            </form>
            <p class="text-left"><a href="homeURL">Home</a></p>
            <%
                String message = (String)request.getAttribute("message");
                if(message == null){
                    message ="";
                }
            %>
            <p style="color:red"><%=message%></p>
        </div>
    </body>
</html>
