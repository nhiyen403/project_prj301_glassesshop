package entity;

public class Customer {
    private int CustomerID;
    private String CustomerName;
    private String Username;
    private String Password;
    private String Address;
    private String Phone;

    public Customer() {
    }

    public Customer(int CustomerID, String CustomerName, String Username, String Password, String Address, String Phone) {
        this.CustomerID = CustomerID;
        this.CustomerName = CustomerName;
        this.Username = Username;
        this.Password = Password;
        this.Address = Address;
        this.Phone = Phone;
    }

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int CustomerID) {
        this.CustomerID = CustomerID;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    @Override
    public String toString() {
        return "Customer{" + "CustomerID=" + CustomerID + ", CustomerName=" + CustomerName + ", Username=" + Username + ", Password=" + Password + ", Address=" + Address + ", Phone=" + Phone + '}';
    }
    
}
