/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Order;
import entity.OrderDetail;
import entity.Product;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Vector;
import model.DAOOrderDetails;
import model.DAOOrders;
import model.DAOProducts;

@WebServlet(name = "orderDetailController", urlPatterns = {"/orderDetailURL"})
public class orderDetailController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            DAOOrderDetails daoOD = new DAOOrderDetails();
            DAOProducts daoPro = new DAOProducts();
            DAOOrders daoO = new DAOOrders();
            HttpSession session = request.getSession();
//            if (service == null) {
//                service = "showCart";
//            }
            String message = request.getParameter("message");
            if (message == null) {
                message = "";
            }
            if (service.equals("showCart")) {
                request.setAttribute("message", message);
                RequestDispatcher dispath = request.getRequestDispatcher("showCart.jsp");
                dispath.forward(request, response);
            }
            // add to Cart
            if (service.equals("addToCart")) {
                String ProductID = request.getParameter("ProductID");
                String[] orderBill = (String[]) session.getAttribute(ProductID);
                if (orderBill == null) {
                    int OrderID = 0;
                    Vector<Product> vecPro = daoPro.getAll("SELECT * FROM Products WHERE ProductID = '" + ProductID + "'");
                    Product pro = vecPro.get(0);
                    double Price = pro.getPrice();
                    int Quantity = 1;
                    OrderDetail od = new OrderDetail(OrderID, ProductID, Quantity, Price);
                    String[] orderBillNew = daoOD.getOrderDetail(od);
                    session.setAttribute(ProductID, orderBillNew);

                } else {
                    int quan = Integer.parseInt(orderBill[2]) + 1;
                    orderBill[2] = String.valueOf(quan);
                    session.setAttribute(ProductID, orderBill);

                }
//                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
//                dispatch.forward(request, response);
                response.sendRedirect("homeURL?message=AddToCartID="+ ProductID+"success!");
            }
            //nhap vao 1 so (neu so do lon hon quatity thi xoa san pham di)
//            if (service.equals("deleteBill")) {
//                String id = request.getParameter("id");
//                Vector<OrderDetail> vector = (Vector<OrderDetail>) session.getAttribute("cart");
//                for (OrderDetail od : vector) {
//
//                    if (od.getProductID() == id && od.getQuantity() > 2) {
//                        vector.remove(od);
//                        session.setAttribute("cart", vector);
//                        break;
//                    }
//                }
//                response.sendRedirect("showCart.jsp");
//            }

            // delete 1 product in cart
//            if (service.equals("deleteBill")) {
//                String id = request.getParameter("id");
//                session.removeAttribute(id);
//                response.sendRedirect("showCart.jsp");
//            }
            // remove All
//            if (service.equals("removeAll")) {
//                java.util.Enumeration em = session.getAttributeNames();
//                while (em.hasMoreElements()) {
//                    String id = em.nextElement().toString();
//                    if (!id.equals("username") && !id.equals("password")) {
//                        session.removeAttribute(id);
//                    }
//                }
//                response.sendRedirect("showCart.jsp");
//            }
            // update cart
            if (service.equals("updateCart")) {
                java.util.Enumeration em = session.getAttributeNames();
                while (em.hasMoreElements()) {
                    String id = em.nextElement().toString();
                    String quantity = request.getParameter(id);
                    if (quantity != null) {
                        int quan = Integer.parseInt(quantity);
                        if (quan <= 0) {
                            session.removeAttribute(id);
                        } else {
                            String[] orderBill = (String[]) session.getAttribute(id);
                            orderBill[2] = quantity;
                            session.setAttribute(id, orderBill);
                        }
                    }
                }
                request.setAttribute("message", message);
                RequestDispatcher dispath = request.getRequestDispatcher("showCart.jsp");
                dispath.forward(request, response);
            }
            // check out 
            if (service.equals("checkOut")) {
                // chua login
                if (session.getAttribute("username") == null) {
                    message = "Please login to check out your cart";
                    request.setAttribute("message", message);
                    RequestDispatcher dispatch = request.getRequestDispatcher("login.jsp");
                    dispatch.forward(request, response);
                } else {
                    // da login
                    String submit = request.getParameter("submit");
                    // tạo order
                    if (submit == null) {
                        response.sendRedirect("orderURL?service=createBill");
                    } else {
                        // đã tạo order
                        java.util.Enumeration em = session.getAttributeNames();
                        while (em.hasMoreElements()) {
                            String id = em.nextElement().toString();
                            if (!id.equals("username") && !id.equals("password")) {
                                //insert database
                                String[] orderBill = (String[]) session.getAttribute(id);
                                Vector<Order> vecOrder = daoO.getAll("SELECT * FROM Orders ORDER BY OrderID DESC");
                                int OrderID = vecOrder.get(0).getOrderID();
                                String ProductID = id;
                                int Quantity = Integer.parseInt(orderBill[2]);
                                double Price = Double.parseDouble(orderBill[3]);
                                OrderDetail orderDetail = new OrderDetail(OrderID, ProductID, Quantity, Price);
                                int n = daoOD.insertOrderDetails(orderDetail);
                                // remove cart
                                session.removeAttribute(id);
                            }
                        }
                        response.sendRedirect("orderDetailURL?service=showCart&message=Check out successful");
                    }
                }
            }
            // display order detail by order id
            if (service.equals("displayOrderDetail")) {
                int orderID = Integer.parseInt(request.getParameter("orderID"));
                Vector<String[]> vec = daoOD.displayOrderDetailByOrder(orderID);
                request.setAttribute("dataOrDe", vec);
                request.setAttribute("tag", "3");
                request.setAttribute("check", "6");
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
