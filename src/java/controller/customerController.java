/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Customer;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Vector;
import model.DAOCustomers;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "customerController", urlPatterns = {"/customerURL"})
public class customerController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try ( PrintWriter out = response.getWriter()) {
            DAOCustomers dao = new DAOCustomers();
            String service = request.getParameter("service");
            HttpSession session = request.getSession();
            String message = request.getParameter("message");
            if(message==null){
                message = "";
            }
            //register
            if (service.equals("addCustomer")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    request.setAttribute("message", message);
                    RequestDispatcher dispath = request.getRequestDispatcher("register.jsp");
                    dispath.forward(request, response);
                } else {
                    String cusName = request.getParameter("cusName");
                    String username = request.getParameter("username");
                    String password = request.getParameter("password");
                    String address = request.getParameter("address");
                    String phone = request.getParameter("phone");
                    Customer cus = new Customer(0, cusName, username, password, address, phone);
                    int n = dao.checkRegister(username);
                    if (n > 0) {
                        dao.insertCustomers(cus);
                        response.sendRedirect("customerURL?service=login");
                    } else {
                        message = "This user name already exsist. Please try again!";
                        request.setAttribute("message", message);
                        request.getRequestDispatcher("register.jsp").forward(request, response);
                    }
                }
            }
            // login
            if (service.equals("login")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    request.setAttribute("message", message);
                    RequestDispatcher dispath = request.getRequestDispatcher("login.jsp");
                    dispath.forward(request, response);
                } else {
                    String name = request.getParameter("username");
                    String pass = request.getParameter("password");
                    // check cai kieu
                    Customer cus = dao.checkLoginCustomer(name, pass);
                    if (cus == null) {
                        message = "This account does not exssist Please try again";
                        request.setAttribute("message", message);
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                    } else {
//                        HttpSession session = request.getSession();
//                        session.setAttribute("cus", cus);
                        session.setAttribute("username", name);
                        session.setAttribute("password", pass);
                        RequestDispatcher dispath = request.getRequestDispatcher("homeURL");
                        dispath.forward(request, response);
                    }
                }
            }
            // logout
            else if (service.equals("logout")) {
                session.invalidate();
                response.sendRedirect("homeURL");
            }
            // display all customer
            if(service.equals("viewAllCustomer")){
                Vector<Customer> vec = dao.getAll("SELECT * FROM Customers");
                request.setAttribute("dataCus", vec);
                request.setAttribute("message", message);
                request.setAttribute("tag", "2");
                request.setAttribute("check", "3");
                RequestDispatcher dispath = request.getRequestDispatcher("homeURL");
                // run
                dispath.forward(request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
