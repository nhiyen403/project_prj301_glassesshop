/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Customer;
import entity.Order;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.ResultSet;
import java.text.ParseException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAOCustomers;
import model.DAOOrderDetails;
import model.DAOOrders;

@WebServlet(name = "orderController", urlPatterns = {"/orderURL"})
public class orderController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            DAOOrders dao = new DAOOrders();
            DAOCustomers daoCus = new DAOCustomers();
            DAOOrderDetails daoOD = new DAOOrderDetails();
            HttpSession session = request.getSession();
            String message = request.getParameter("message");
            if (message == null) {
                message = "";
            }
            if (service.equals("createBill")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    String username = (String) session.getAttribute("username");
                    String password = (String) session.getAttribute("password");
                    Vector<Customer> vecCus = daoCus.getAll("SELECT * FROM Customers WHERE Username = '" + username + "' AND Password ='" + password + "'");
                    request.setAttribute("message", message);
                    request.setAttribute("vecCus", vecCus);
                    RequestDispatcher dispath = request.getRequestDispatcher("createBill.jsp");
                    dispath.forward(request, response);
                } else {
                    int CustomerID = Integer.parseInt(request.getParameter("CustomerID"));
                    String OrderDate = request.getParameter("OrderDate");
                    String status = "Wait";
                    Order order = new Order(0, CustomerID, OrderDate, status);
                    int n = dao.insertOrder(order);
                    if (n > 0) {
                        response.sendRedirect("orderDetailURL?service=checkOut&submit=createBill");
//                        Vector<Order> vec = dao.getAll("Select * from Orders ORDER BY OrderID DESC");
//                        int orderID = vec.get(0).getOrderID();
//                        message = "Input information of order detail";
//                        response.sendRedirect("OrderDetailURL?service=insertBill&orderID="+orderID+"&message=" + message);
                    } else {
                        message = "Fail to create a bill";
                        response.sendRedirect("orderURL?service=createBill&message=" + message + "&CustomerID=" + CustomerID);
                    }
                }
            }
            // display all order
            if (service.equals("viewAllOrder")) {
                Vector<String[]> vec = dao.getAllOrder("SELECT o.OrderID, c.CustomerName, o.OrderDate, o.[Status], SUM(od.Quantity * p.Price) "
                        + "FROM Products p INNER JOIN OrderDetails od ON p.ProductID = od.ProductID "
                        + "INNER JOIN Orders o ON od.OrderID = o.OrderID "
                        + "INNER JOIN Customers c ON c.CustomerID = o.CustomerID "
                        + "GROUP BY o.OrderID, c.CustomerName, o.OrderDate, o.[Status]");
                request.setAttribute("dataOr", vec);
                request.setAttribute("tag", "3");
                request.setAttribute("check", "5");
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
            // update status of order
            if (service.equals("updateOrder")) {
                int OrderID = Integer.parseInt(request.getParameter("OrderID"));
                String status = request.getParameter("status");
                String checkDone = request.getParameter("checkDone");
                if (!checkDone.equals("Done")) {
                    Vector<Order> vector = dao.getAll("SELECT * FROM Orders WHERE OrderID = " + OrderID);
                    Order order = vector.get(0);
                    order.setStatus(status);
                    int n = dao.updateOrder(order);
                    Vector<String[]> vec = daoOD.displayOrderDetailByOrder(OrderID);
                    request.setAttribute("dataOrDe", vec);
                    request.setAttribute("tag", "3");
                    request.setAttribute("check", "6");
                    message = "Update status successful";
                    request.setAttribute("message", message);
                    RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                    dispatch.forward(request, response);
                } else {
                    Vector<String[]> vec = daoOD.displayOrderDetailByOrder(OrderID);
                    request.setAttribute("dataOrDe", vec);
                    request.setAttribute("tag", "3");
                    request.setAttribute("check", "6");
                    message = "Status has done. Can not change status!";
                    request.setAttribute("message", message);
                    RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                    dispatch.forward(request, response);
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(orderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(orderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
