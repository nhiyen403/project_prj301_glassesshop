/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Admin;
import entity.Customer;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.DAOAdmins;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "adminController", urlPatterns = {"/adminURL"})
public class adminController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("adminLogin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            String message = request.getParameter("message");
            DAOAdmins dao = new DAOAdmins();
            HttpSession session = request.getSession();
            if (message == null) {
                message = "";
            }
            // login
            if (service.equals("login")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    request.setAttribute("message", message);
                    RequestDispatcher dispath = request.getRequestDispatcher("login.jsp");
                    dispath.forward(request, response);
                } else {
                    String name = request.getParameter("username");
                    String pass = request.getParameter("password");
                    // check cai kieu
                    Admin ad = dao.checkLoginAdmin(name, pass);
                    if (ad == null) {
                        message = "This account does not exssist Please try again";
                        request.setAttribute("message", message);
                        request.getRequestDispatcher("adminLogin.jsp").forward(request, response);
                    } else {
                        session.setAttribute("username", name);
                        session.setAttribute("password", pass);
                        request.setAttribute("check", "isAdmin");
                        RequestDispatcher dispath = request.getRequestDispatcher("/homeURL");
                        dispath.forward(request, response);
                    }
                }
            }
            // logout
            if (service.equals("logout")) {
                session.invalidate();
                response.sendRedirect("homeURL");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
