/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Product;
import entity.Supplier;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.util.Vector;
import model.DAOProducts;
import model.DAOSuppliers;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "productController", urlPatterns = {"/productURL"})
public class productController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            DAOProducts daoPro = new DAOProducts();
            DAOSuppliers daoSup = new DAOSuppliers();
            String message = request.getParameter("message");
            if (message == null) {
                message = "";
            }
            if (service.equals("searchProduct")) {
                String ProductName = request.getParameter("ProductName");
                Vector<String[]> vec = daoPro.getCateSupPro(
                        "SELECT p.ProductID, p.ProductName, p.Price, p.Status, "
                        + "c.CategoryName, s.CompanyName, c.Description, c.Picture "
                        + "FROM Products p "
                        + "INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                        + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID "
                        + "WHERE p.ProductName LIKE '%" + ProductName + "%'");
                request.setAttribute("dataPro", vec);
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
            // display product by supplier
            if (service.equals("viewProductBySup")) {
                int supID = Integer.parseInt(request.getParameter("SupplierID"));
                Vector<String[]> vecPro = daoPro.getCateSupPro(
                        "SELECT p.ProductID, p.ProductName, p.Price, p.Status, "
                        + "c.CategoryName, s.CompanyName, c.Description, c.Picture  "
                        + "FROM Products p "
                        + "INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                        + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID "
                        + "WHERE s.SupplierID = " + supID);
                request.setAttribute("dataPro", vecPro);
                request.setAttribute("tag", request.getParameter("SupplierID"));
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
            // display product detail
            if (service.equals("showProductDetail")) {
                String proID = request.getParameter("productID");
                Vector<String[]> vecPro = daoPro.showProductDetail(proID);

                request.setAttribute("dataPro", vecPro);
                request.setAttribute("check", "1");
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
            // display all product
            if (service.equals("viewAllProduct")) {
                Vector<String[]> vec = daoPro.getCateSupPro(
                        "SELECT p.ProductID, p.ProductName, p.Price, p.Status, "
                        + "c.CategoryName, s.CompanyName, c.Description, c.Picture "
                        + "FROM Products p "
                        + "INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                        + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID ");
                request.setAttribute("message", message);
                request.setAttribute("dataPro", vec);
                request.setAttribute("tag", "1");
                request.setAttribute("check", "4");
                RequestDispatcher dispatch = request.getRequestDispatcher("homeURL");
                dispatch.forward(request, response);
            }
            // insert product

            if (service.equals("insertProduct")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    ResultSet rsCate
                            = daoPro.getData("select CategoryID from Categories");
                    ResultSet rsSup
                            = daoPro.getData("select SupplierID from Suppliers");
                    //send record to form
                    request.setAttribute("rsCate", rsCate);
                    request.setAttribute("rsSup", rsSup);
                    RequestDispatcher dispath
                            = request.getRequestDispatcher("insertProduct.jsp");
                    dispath.forward(request, response);
                } else {
                    String ProductID = request.getParameter("ProductID");
                    String ProductName = request.getParameter("ProductName");
                    String Price = request.getParameter("Price");
                    String Status = request.getParameter("Status");
                    String CategoryID = request.getParameter("CategoryID");
                    String SupplierID = request.getParameter("SupplierID");

                    //convert
                    double price = Double.parseDouble(Price);
                    int status = Integer.parseInt(Status);
                    int cate = Integer.parseInt(CategoryID);
                    int sup = Integer.parseInt(SupplierID);

                    //create object
                    Product pro = new Product(ProductID, ProductName, price, status, cate, sup);
                    int n = daoPro.insertProducts(pro);
                    response.sendRedirect("productURL");

                }
            }
            //update product
            if (service.equals("updateProduct")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    int ProductID = Integer.parseInt(request.getParameter("ProductID"));
                    Vector<Product> vector
                            = daoPro.getAll("select * from Products where ProductID=" + ProductID);
                    ResultSet rsCate
                            = daoPro.getData("select CategoryID from Categories");
                    ResultSet rsSup
                            = daoPro.getData("select SupplierID from Suppliers");
                    //send record to form
                    request.setAttribute("data", vector);
                    request.setAttribute("rsCate", rsCate);
                    request.setAttribute("rsSup", rsSup);
                    RequestDispatcher dispath
                            = request.getRequestDispatcher("updateProduct.jsp");
                    dispath.forward(request, response);
                } else {
                    String ProductID = request.getParameter("ProductID");
                    String ProductName = request.getParameter("ProductName");
                    String Price = request.getParameter("Price");
                    String Status = request.getParameter("Status");
                    String CategoryID = request.getParameter("CategoryID");
                    String SupplierID = request.getParameter("SupplierID");

                    //convert
                    double price = Double.parseDouble(Price);
                    int status = Integer.parseInt(Status);
                    int cate = Integer.parseInt(CategoryID);
                    int sup = Integer.parseInt(SupplierID);

                    //create object
                    Product pro = new Product(ProductID, ProductName, price, status, cate, sup);
                    response.sendRedirect("productURL");

                }
            }
            // delete product
            if (service.equals("deleteProduct")) {
                String productID = request.getParameter("ProductID");
                int n = daoPro.deleteProduct(productID);
                message = "Failed to delete product";
                response.sendRedirect("productURL?message=" + message);
            }
            
            // search by price
            if (service.equals("searchByPrice")) {
                String min = request.getParameter("MinPrice").trim();
                String max = request.getParameter("MaxPrice").trim();
                if (min.isEmpty() || max.isEmpty() /* || min==null || max == null*/) {
                    message = "Empty value please input again";
                    response.sendRedirect("productURL?service=viewAllProduct&message=" + message);
                } else {
                    double minPrice;
                    double maxPrice;
                    // check format cua input
                    try {
                        minPrice = Double.parseDouble(min);
                        maxPrice = Double.parseDouble(max);
                        // check min < max
                        if (minPrice > maxPrice) {
                            message = "Input again because min price lower max price";
                            response.sendRedirect("productURL?service=viewAllProduct&message=" + message);
                        } else {
                            Vector<String[]> vec = daoPro.getCateSupPro("SELECT p.ProductID, p.ProductName, p.Price, p.Status, c.CategoryName, s.CompanyName, c.Description, c.Picture  "
                                    + "FROM Products p INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                                    + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID "
                                    + "WHERE p.Status = 1 AND p.Price between " + minPrice + " and " + maxPrice);
                            // set data cho view 
                            request.setAttribute("dataPro", vec);
                            request.setAttribute("tag", "1");
                            request.setAttribute("check", "4");
                            RequestDispatcher dispath = request.getRequestDispatcher("homeURL");
                            dispath.forward(request, response);
                        }
                    } catch (NumberFormatException ex) {
                        message = "Wrong input, please input again";
                        response.sendRedirect("productURL?service=viewAllProduct&message=" + message);
                    }
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
