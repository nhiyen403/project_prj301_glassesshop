package controller;

import entity.Customer;
import entity.Supplier;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Vector;
import model.DAOProducts;
import model.DAOSuppliers;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "homeController", urlPatterns = {"/homeURL"})
public class homeController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            //String service = request.getParameter("service");
            DAOSuppliers daoSup = new DAOSuppliers();
            DAOProducts daoPro = new DAOProducts();
            Vector<Supplier> vecSup = (Vector) request.getAttribute("dataSup");
            if (vecSup == null) {
                vecSup = daoSup.getAll("SELECT * FROM Suppliers");
            }
            Vector<String[]> vecPro = (Vector) request.getAttribute("dataPro");
            if (vecPro == null) {
                vecPro = daoPro.getCateSupPro("SELECT p.ProductID, p.ProductName, p.Price, p.Status, c.CategoryName, s.CompanyName, c.Description, c.Picture  "
                        + "FROM Products p INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                        + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID "
                        + "WHERE p.Status = 1");
            }
            Vector<Customer> vecCus = (Vector) request.getAttribute("dataCus");
            Vector<String[]> vecOr = (Vector) request.getAttribute("dataOr");
            Vector<String[]> vecOrDe = (Vector) request.getAttribute("dataOrDe");
            
            String tag = (String) request.getAttribute("tag");
            if (tag == null) {
                tag = "0";
            }
            String check = (String) request.getAttribute("check");
            if (check == null) {
                check = "0";
            }
            String message = (String)request.getAttribute("message");
            if(message == null){
                message="";
            }
            
            request.setAttribute("message", message);
            request.setAttribute("dataSup", vecSup);
            request.setAttribute("dataPro", vecPro);
            request.setAttribute("dataCus", vecCus);
            request.setAttribute("dataOr", vecOr);
            request.setAttribute("dataOrDe", vecOrDe);
            request.setAttribute("tag", tag);
            request.setAttribute("check", check);
            RequestDispatcher dispatch = request.getRequestDispatcher("index.jsp");
            dispatch.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
