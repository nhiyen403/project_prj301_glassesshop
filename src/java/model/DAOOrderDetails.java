/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.OrderDetail;
import entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOOrderDetails extends DBConnect {

    public int insertOrderDetails(OrderDetail od) {
        int n = 0;

        String sql = "INSERT INTO [dbo].[OrderDetails]\n"
                + "           ([OrderID]\n"
                + "           ,[ProductID]\n"
                + "           ,[Quantity]\n"
                + "           ,[Price])\n"
                + "     VALUES\n"
                + "(" + od.getOrderID()
                + ",'" + od.getProductID()
                + "'," + od.getQuantity()
                + "," + od.getPrice() + ")";
        System.out.println(sql);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrderDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int updateOrderDetails(OrderDetail od) {
        int n = 0;
        String sql = "UPDATE [dbo].[OrderDetails]\n"
                + "   SET [Quantity] = ?"
                + "      ,[Price] = ?"
                + " WHERE OrderID = ? AND ProductID = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, od.getQuantity());
            pre.setDouble(2, od.getPrice());
            pre.setInt(3, od.getOrderID());
            pre.setString(4, od.getProductID());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public int deleteOrderDetails(int orID, int proID) {
        int n = 0;
        String sql = "DELETE from [Order Details] WHERE OrderID = " + orID + " AND ProductID = '" + proID + "'";
        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public Vector<OrderDetail> getAll(String sql) {
        Vector<OrderDetail> vec = new Vector<>();
        Statement state;
        try {
            state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                int orID = rs.getInt(1);
                String proID = rs.getString(2);
                int quan = rs.getInt(3);
                double price = rs.getDouble(4);

                OrderDetail od = new OrderDetail(orID, proID, quan, price);
                vec.add(od);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vec;
    }

    public String[] getOrderDetail(OrderDetail od) {
        String[] orderDetail = new String[4];
        DAOProducts daoPro = new DAOProducts();
        Vector<Product> product = daoPro.getAll("SELECT * FROM Products WHERE ProductID = '" + od.getProductID() + "'");
        orderDetail[0] = String.valueOf(od.getProductID());// productID
        orderDetail[1] = product.get(0).getProductName();// productname
        orderDetail[2] = String.valueOf(od.getQuantity());// quantity
        orderDetail[3] = String.valueOf(od.getPrice());// unit price
        return orderDetail;
    }

    public Vector<String[]> displayOrderDetailByOrder(int OrderID) {
        Vector<String[]> list = new Vector<>();
        String sql = "SELECT o.OrderID, o.OrderDate, c.CustomerName, p.ProductName, od.Quantity, p.Price, od.Quantity * p.Price, o.Status "
                + "FROM Products p "
                + "INNER JOIN OrderDetails od ON p.ProductID = od.ProductID "
                + "INNER JOIN Orders o ON od.OrderID = o.OrderID "
                + "INNER JOIN Customers c ON c.CustomerID = o.CustomerID "
                + "WHERE o.OrderID = " + OrderID;
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                String[] row = new String[8];
                int Orderid = rs.getInt(1);
                String orDate = rs.getDate(2).toString();
                String cusName = rs.getString(3);
                String proName = rs.getString(4);
                String quan = rs.getString(5);
                String price = rs.getString(6);
                String total = rs.getString(7);
                String status = rs.getString(8);

                row[0] = String.valueOf(Orderid);
                row[1] = orDate;
                row[2] = cusName;
                row[3] = proName;
                row[4] = quan;
                row[5] = price;
                row[6] = total;
                row[7] = status;
                list.add(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOOrderDetails dao = new DAOOrderDetails();
    }
}
