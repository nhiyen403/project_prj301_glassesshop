/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Admin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOAdmins extends DBConnect {

    public Admin checkLoginAdmin(String username, String password) {
        String sql = "SELECT * FROM Admins WHERE Username = ? AND Password = ?";
        Admin admin = null;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, username);
            pre.setString(2, password);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                String adminName = rs.getString(1);
                String adminPass = rs.getString(2);
                admin = new Admin(adminName, adminPass);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return admin;
    }
    
}
