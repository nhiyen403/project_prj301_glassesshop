/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Customer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOCustomers extends DBConnect {

    public int insertCustomers(Customer cus) {
        int n = 0;
        String sql = "INSERT INTO [dbo].[Customers]\n"
                + "            ([CustomerName]\n"
                + "           ,[Username]\n"
                + "           ,[Password]\n"
                + "           ,[Address]\n"
                + "           ,[Phone])\n"
                + "     VALUES\n"
                + "(N'" + cus.getCustomerName()
                + "','" + cus.getUsername()
                + "','" + cus.getPassword()
                + "','" + cus.getAddress()
                + "','" + cus.getPhone() + "')";
        System.out.println(sql);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomers.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int updateCustomer(Customer cus) {
        int n = 0;

        String sql = "UPDATE [dbo].[Customers]"
                + "   SET [CustomerName] = ?"
                + "      ,[Username] = ?"
                + "      ,[Password] = ?"
                + "      ,[Address] = ?"
                + "      ,[Phone] = ?"
                + " WHERE [CustomerID] = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, cus.getCustomerName());
            pre.setString(2, cus.getUsername());
            pre.setString(3, cus.getPassword());
            pre.setString(4, cus.getAddress());
            pre.setString(5, cus.getPhone());
            pre.setInt(6, cus.getCustomerID());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomers.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int deleteCustomer(int id) {
        int n = 0;
        String sql = "DELETE FROM Customers WHERE CustomerID = " + id;

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomers.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public Vector<Customer> getAll(String sql) {
        Vector<Customer> vec = new Vector<>();

        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt(1);
                String cusName = rs.getString(2);
                String username = rs.getString(3);
                String pass = rs.getString(4);
                String add = rs.getString(5);
                String phone = rs.getString(6);

                Customer cus = new Customer(id, cusName, username, pass, add, phone);
                vec.add(cus);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vec;
    }

    public Customer checkLoginCustomer(String username, String password) {
        String sql = "SELECT * FROM Customers WHERE Username = ? AND Password = ?";
        Customer cus = null;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, username);
            pre.setString(2, password);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String cusName = rs.getString(2);
                String add = rs.getString(5);
                String phone = rs.getString(6);
                cus = new Customer(id, cusName, username, password, add, phone);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cus;
    }

    public int checkRegister(String username) {
        int n = 1;
        Vector<Customer> vector = getAll("SELECT * FROM Customers");
        for (Customer customer : vector) {
            if (customer.getUsername().equals(username)) {
                n = 0;
                break;
            }
        }
        return n;
    }

    public static void main(String[] args) {
        DAOCustomers dao = new DAOCustomers();

//        Vector<Customer> vec = dao.getAll("SELECT * FROM Customers");
//        for (Customer customer : vec) {
//            System.out.println(customer);
//        }
        Customer cus = dao.checkLoginCustomer("DuongTD", "123456");
        System.out.println(cus);
    }
}
