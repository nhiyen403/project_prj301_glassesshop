package model;

import entity.Category;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOCategories extends DBConnect {

    public int insertCategory(Category cat) {
        int n = 0;
        String sql = "INSERT INTO [Categories]"
                + "           ([CategoryName]"
                + "           ,[Description]"
                + "           ,[Picture])"
                + "     VALUES"
                + "           ('" + cat.getCategoryName() + "','" + cat.getDescription() + "'," + cat.getPicture() + ")";

        System.out.println(sql);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategories.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int insertCategoryByPrepared(Category cat) {
        int n = 0;
        String sql = "INSERT INTO [Categories]\n"
                + "           ([CategoryName]\n"
                + "           ,[Description]\n"
                + "           ,[Picture])\n"
                + "     VALUES\n"
                + "           (?,?,?)";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, cat.getCategoryName());
            pre.setString(2, cat.getDescription());
            byte[] pic = cat.getPicture().getBytes();
            pre.setBytes(3, pic);
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategories.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public int updateCategory(Category cat) {
        int n = 0;
        String sql = "UPDATE [dbo].[Categories]\n"
                + "   SET [CategoryName] = ?"
                + "      ,[Description] = ?"
                + "      ,[Picture] = ?"
                + " WHERE CategoryID = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, cat.getCategoryName());
            pre.setString(2, cat.getDescription());
            pre.setString(3, cat.getPicture());
            pre.setInt(4, cat.getCategoryID());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategories.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int deleteCategory(int id) {
        int n = 0;
        String sql = "DELETE FROM Categories WHERE CategoryID =" + id;

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategories.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Vector<Category> getAll(String sql) {
        Vector<Category> vec = new Vector<>();
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE, 
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String des = rs.getString(3);
                String pic = rs.getString(4);

                Category cat = new Category(id, name, des, pic);
                vec.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategories.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vec;
    }

    public static void main(String[] args) {
        DAOCategories dao = new DAOCategories();

        Vector<Category> vec = dao.getAll("SELECT * FROM Categories");
        for (Category category : vec) {
            System.out.println(category);
        }
    }

}
