/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Order;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOOrders extends DBConnect {

    public int insertOrder(Order order) {
        int n = 0;

        String sql = "INSERT INTO [dbo].[Orders]"
                + "            ([CustomerID]"
                + "           ,[OrderDate]"
                + "           ,[Status])\n"
                + "     VALUES"
                + "(" + order.getCustomerID()
                + ",'" + order.getOrderDate()
                + "','" + order.getStatus() + "')";
        System.out.println(sql);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public int updateOrder(Order o) throws ParseException {
        int n = 0;
        String sql = "UPDATE [dbo].[Orders]\n"
                + "   SET [CustomerID] = ?"
                + "      ,[OrderDate] = ?"
                + "      ,[Status] = ?"
                + " WHERE OrderID = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, o.getCustomerID());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date sqlOrderDate = new java.sql.Date(formatter.parse(o.getOrderDate()).getTime());
            pre.setDate(2, sqlOrderDate);
            pre.setString(3, o.getStatus());
            pre.setInt(4, o.getOrderID());

            n = pre.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return n;
    }

    public int deleteOrder(int id) {
        int n = 0;
        String sql = "DELETE from Orders WHERE OrderID = " + id;
        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Vector<Order> getAll(String sql) {
        Vector<Order> vec = new Vector<>();

        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt(1);
                int cusID = rs.getInt(2);
                String orDate = rs.getDate(3).toString();
                Order or = new Order(id, cusID, orDate);
                vec.add(or);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vec;
    }

    public Vector<Order> searchOrderByCus(int id) {
        Vector<Order> vector = new Vector<>();
        String sql = "SELECT * FROM Orders WHERE CustomerID = " + id;
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);
            while (rs.next()) {
                int OrderID = rs.getInt(1);
                int CustomerID = rs.getInt(2);
                String OrderDate = rs.getDate(3).toString();
                Order or = new Order(OrderID, CustomerID, OrderDate);
                vector.add(or);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<String[]> getAllOrder(String sql) {
        Vector<String[]> list = new Vector<>();
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                String[] row = new String[5];
                int Orderid = rs.getInt(1);
                String cusName = rs.getString(2);
                String orDate = rs.getDate(3).toString();
                String status = rs.getString(4);
                double total = rs.getDouble(5);

                row[0] = String.valueOf(Orderid);
                row[1] = cusName;
                row[2] = orDate;
                row[3] = status;
                row[4] = String.valueOf(total);
                list.add(row);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        DAOOrders dao = new DAOOrders();
   
//        Vector<String[]> vec = dao.getAllOrder("SELECT o.OrderID, c.CustomerName, o.OrderDate, o.[Status], SUM(od.Quantity * p.Price) "
//                        + "FROM Products p INNER JOIN OrderDetails od ON p.ProductID = od.ProductID "
//                        + "INNER JOIN Orders o ON od.OrderID = o.OrderID "
//                        + "INNER JOIN Customers c ON c.CustomerID = o.CustomerID ");
//        for (String[] strings : vec) {
//            System.out.println(Arrays.toString(strings));
//        }
    }
}
