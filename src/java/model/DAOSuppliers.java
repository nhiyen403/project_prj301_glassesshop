/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Supplier;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOSuppliers extends DBConnect {

    public int insertSuppliers(Supplier sup) {
        int n = 0;

        String sql = "INSERT INTO [dbo].[Suppliers]\n"
                + "           ([SupplierID]\n"
                + "           ,[CompanyName]\n"
                + "           ,[Address]\n"
                + "           ,[Phone])\n"
                + "     VALUES"
                + "(" + sup.getSupplierID()
                + ",'" + sup.getCompanyName()
                + "','" + sup.getAddress()
                + "','" + sup.getPhone() + "')";
        System.out.println(sql);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return n;
    }

    public int updateSupplier(Supplier sup) {
        int n = 0;

        String sql = " UPDATE [dbo].[Suppliers]\n"
                + "   SET [CompanyName] = ?"
                + "      ,[Address] = ?"
                + "      ,[Phone] = ?"
                + " WHERE SupplierID = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, sup.getCompanyName());
            pre.setString(2, sup.getAddress());
            pre.setString(3, sup.getPhone());
            pre.setInt(4, sup.getSupplierID());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return n;
    }

    public int deleteProduct(int id) {
        int n = 0;
        String sql = "DELETE from Suppliers WHERE SupplierID = " + id;
        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOProducts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Vector<Supplier> getAll(String sql) {
        Vector<Supplier> vec = new Vector<>();
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt(1);
                String comName = rs.getString(2);
                String add = rs.getString(3);
                String phone = rs.getString(4);

                Supplier sup = new Supplier(id, comName, add, phone);
                vec.add(sup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOSuppliers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vec;
    }

    public static void main(String[] args) {
        DAOSuppliers dao = new DAOSuppliers();

        Vector<Supplier> vec = dao.getAll("SELECT * FROM Suppliers");
        for (Supplier supplier : vec) {
            System.out.println(supplier);
        }
    }
}
