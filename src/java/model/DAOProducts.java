/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOProducts extends DBConnect {

    public int insertProducts(Product pro) {
        int n = 0;
        String spl = "INSERT INTO [dbo].[Products]\n"
                + "           ([ProductID]\n"
                + "           ,[ProductName]\n"
                + "           ,[Price]\n"
                + "           ,[Status]\n"
                + "           ,[CategoryID]\n"
                + "           ,[SupplierID])\n"
                + "     VALUES\n"
                + "('" + pro.getProductID()
                + "','" + pro.getProductName()
                + "," + pro.getPrice()
                + "," + pro.getStatus()
                + "," + pro.getCategoryID()
                + "," + pro.getSupplierID() + ")";
        System.out.println(spl);

        try {
            Statement state = conn.createStatement();
            n = state.executeUpdate(spl);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public int updateProduct(Product pro) {
        int n = 0;
        String sql = "UPDATE [dbo].[Products]\n"
                + "   SET [ProductName] = ?"
                + "      ,[Price] = ?"
                + "      ,[Status] =?"
                + "      ,[CategoryID] = ?"
                + "      ,[SupplierID] = ?"
                + " WHERE     ProductID = ?";

        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, pro.getProductName());
            pre.setDouble(2, pro.getPrice());
            pre.setInt(3, pro.getStatus());
            pre.setInt(4, pro.getSupplierID());
            pre.setInt(5, pro.getCategoryID());
            pre.setString(6, pro.getProductID());
            n = pre.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public int deleteProduct(String id) {
        int n = 0;
        String sql = "DELETE from Products WHERE ProductID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, id);
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProducts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Vector<Product> getAll(String sql) {
        Vector<Product> vector = new Vector<>();
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                String id = rs.getString(1);
                String pname = rs.getString(2);
                Double price = rs.getDouble(3);
                int status = rs.getInt(4);
                int supID = rs.getInt(5);
                int cateID = rs.getInt(6);
                Product pro = new Product(id, pname, price, status, cateID, supID);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vector;
    }

    public Vector<Product> searchByName(String name) {
        Vector<Product> vector = new Vector<>();
        String sql = "SELECT * FROM Products WHERE ProductName LIKE '%" + name + "%'";
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                String id = rs.getString(1);
                String pname = rs.getString(2);
                Double price = rs.getDouble(3);
                int status = rs.getInt(4);
                int supID = rs.getInt(5);
                int cateID = rs.getInt(6);
                Product pro = new Product(id, pname, price, status, cateID, supID);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProducts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<String[]> getCateSupPro(String sql) {
        Vector<String[]> list = new Vector<>();
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);
            while (rs.next()) {
                String[] row = new String[8];
                String proID = rs.getString(1);
                String pname = rs.getString(2);
                Double price = rs.getDouble(3);
                int status = rs.getInt(4);
                String cateName = rs.getString(5);
                String compName = rs.getString(6);
                String des = rs.getString(7);
                String pic = rs.getString(8);

                row[0] = proID;
                row[1] = pname;
                row[2] = String.valueOf(price);
                row[3] = String.valueOf(status);
                row[4] = cateName;
                row[5] = compName;
                row[6] = des;
                row[7] = pic;
                list.add(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProducts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Vector<String[]> showProductDetail(String proID) {
        Vector<String[]> vector = new Vector<>();
        String sql = "SELECT p.ProductID, p.ProductName, p.Price, p.Status, c.CategoryName, s.CompanyName, c.Description, c.Picture "
                + "FROM Products p INNER JOIN Categories c ON p.CategoryID = c.CategoryID "
                + "INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID "
                + "WHERE p.ProductID='" + proID + "'";
        try {
            Statement state = conn.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);

            while (rs.next()) {
                String[] row = new String[8];
                String pname = rs.getString(2);
                Double price = rs.getDouble(3);
                int status = rs.getInt(4);
                String cateName = rs.getString(5);
                String compName = rs.getString(6);
                String des = rs.getString(7);
                String pic = rs.getString(8);

                row[0] = proID;
                row[1] = pname;
                row[2] = String.valueOf(price);
                row[3] = String.valueOf(status);
                row[4] = cateName;
                row[5] = compName;
                row[6] = des;
                row[7] = pic;

                vector.add(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProducts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public static void main(String[] args) {
        DAOProducts dao = new DAOProducts();
        //Select
//        Vector<Product> vector = dao.getAll("SELECT * FROM Products");
//        for (Product product : vector) {
//            System.out.println(product);
//        }
        //search
//        Vector<Product> vector = dao.searchByName("1");
//        for (Product product : vector) {
//            System.out.println(product);
//        }
        // inner join
//        Vector<String[]>list = dao.getCateSupPro("SELECT p.ProductID, p.ProductName, p.Price, p.Status, c.CategoryName, s.CompanyName  
//                                                  FROM Products p INNER JOIN Categories c ON p.CategoryID = c.CategoryID 
//                                                  INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID");
//        for (String[] strings : list) {
//            System.out.println(Arrays.toString(strings));
//        }
    }
}
